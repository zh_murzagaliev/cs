package ru.sgu.interceptor;


import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.util.logging.Logger;


@SuppressWarnings("unused")
@Interceptor
@Log
public class LoggingInterceptor {
    private static final Logger logger = Logger.getLogger(LoggingInterceptor.class.getName());

    @AroundInvoke
    public Object intercept(InvocationContext ctx) throws Exception {
        logger.info("--before method invoke--: " + ctx.getMethod().getName());
        Object result = ctx.proceed();
        logger.info("--after  method invoke--: " + ctx.getMethod().getName());
        return result;
    }
}
