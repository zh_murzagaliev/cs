package ru.sgu.crypto;

import javax.ejb.Local;
import java.io.File;

@Local
public interface Crypto {
    public void cryptographyAES(String pathSource, String pathDestination, byte [] key, int mode) ;
    public void cryptographyAES(String pathSource, String pathDestination, String key, int mode) ;
    public byte[] getMD5File(File file);
}
