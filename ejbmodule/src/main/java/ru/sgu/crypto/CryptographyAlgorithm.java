package ru.sgu.crypto;


public enum  CryptographyAlgorithm {
    DES, AES_256
}
