package ru.sgu.crypto;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class Cryptography implements Crypto {

    private final Logger LOGGER = Logger.getLogger(Cryptography.class.getName());

    public void cryptographyAES(String pathSource, String pathDestination, byte [] key, int mode) {
        Cipher cipher = getCipher("AES");

        if (cipher == null) {
            return;
        }

        SecretKey secretKey = new SecretKeySpec(key, "AES");
        try {
            cipher.init(mode, secretKey);
        } catch (InvalidKeyException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

        File sourceFile = new File(pathSource);

        byte [] contents = new byte[(int) sourceFile.length()];

        try (FileInputStream in = new FileInputStream(pathSource);) {
            in.read(contents);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }

        try (FileOutputStream fout = new FileOutputStream(pathDestination);
             CipherOutputStream out = new CipherOutputStream(fout, cipher)) {
             out.write(contents);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    public void cryptographyAES(String pathSource, String pathDestination, String key, int mode) {
        byte[] temp = getMD5(key);
        cryptographyAES(pathSource, pathDestination, temp, mode);
    }

    private byte[] getMD5(String string) {
        return getMD5(string.getBytes());
    }

    private byte[] getMD5(byte [] bytes) {
        MessageDigest digest = getMD5Digest();
        return digest == null? null: digest.digest(bytes);
    }

    private MessageDigest getMD5Digest(){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        return digest;
    }

    private Cipher getCipher(String algorithm) {
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance(algorithm);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
        return cipher;
    }

    public byte[] getMD5File(File file) {
        byte [] md5 = null;
        try(FileInputStream in = new FileInputStream(file)) {
            byte [] contents = new byte[(int) file.length()];
            in.read(contents);
            md5 = getMD5(contents);
        } catch (FileNotFoundException e) {
            System.out.println("Error:" + e );
        } catch (IOException e) {
            System.out.println("Error:" + e);
        }
        return md5;
    }

}