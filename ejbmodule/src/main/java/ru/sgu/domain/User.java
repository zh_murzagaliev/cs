package ru.sgu.domain;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.findAllByUserName", query = "SELECT u FROM User u WHERE u.userName = ?" ),
})
@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_SEQ")
    @SequenceGenerator(name="USER_SEQ", sequenceName="USER_SEQ", allocationSize=1)
    @Column(name = "user_id")
    Long id;

    @Size(max = 255)
    @Column(name = "user_name", unique = true)
    private String userName;

    @Size(max = 512)
    @Column(name = "user_password")
    private String userPassword;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_creating_password")
    private Date dateCreatingPassword;

    @Embedded
    Role role;

    public User(String userName, String userPassword, Date dateCreatingPassword, Role role) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.dateCreatingPassword = dateCreatingPassword;
        this.role = role;
    }

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreatingPassword() {
        return dateCreatingPassword;
    }

    public void setDateCreatingPassword(Date dateCreatingPassword) {
        this.dateCreatingPassword = dateCreatingPassword;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
