package ru.sgu.domain;


import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class Role implements Serializable {

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private RoleType userRole;

    public Role() {
    }

    public Role(RoleType userRole) {
        this.userRole = userRole;
    }

    public RoleType getUserRole() {
        return userRole;
    }

    public void setUserRole(RoleType userRole) {
        this.userRole = userRole;
    }
}
