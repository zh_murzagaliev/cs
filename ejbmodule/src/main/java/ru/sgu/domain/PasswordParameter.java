package ru.sgu.domain;

import java.util.Date;

public class PasswordParameter {

    private Date maxLimitPasswordAction;
    private Date minLimitPasswordAction;
    private int minLengthPassword;
    private String regex;
    private boolean isUniquePassword;
    private int speedOfPasswordGuessing;

    public PasswordParameter(Date maxLimitPasswordAction, Date minLimitPasswordAction,
                             int minLengthPassword, String regex, boolean uniquePassword, int speedOfPasswordGuessing) {

        this.maxLimitPasswordAction = maxLimitPasswordAction;
        this.minLimitPasswordAction = minLimitPasswordAction;
        this.minLengthPassword = minLengthPassword;
        this.regex = regex;
        isUniquePassword = uniquePassword;
        this.speedOfPasswordGuessing = speedOfPasswordGuessing;
    }

    public Date getMaxLimitPasswordAction() {
        return maxLimitPasswordAction;
    }

    public void setMaxLimitPasswordAction(Date maxLimitPasswordAction) {
        this.maxLimitPasswordAction = maxLimitPasswordAction;
    }

    public Date getMinLimitPasswordAction() {
        return minLimitPasswordAction;
    }

    public void setMinLimitPasswordAction(Date minLimitPasswordAction) {
        this.minLimitPasswordAction = minLimitPasswordAction;
    }

    public int getMinLengthPassword() {
        return minLengthPassword;
    }

    public void setMinLengthPassword(int minLengthPassword) {
        this.minLengthPassword = minLengthPassword;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public boolean isUniquePassword() {
        return isUniquePassword;
    }

    public void setUniquePassword(boolean uniquePassword) {
        isUniquePassword = uniquePassword;
    }

    public int getSpeedOfPasswordGuessing() {
        return speedOfPasswordGuessing;
    }

    public void setSpeedOfPasswordGuessing(int speedOfPasswordGuessing) {
        this.speedOfPasswordGuessing = speedOfPasswordGuessing;
    }

    @Override
    public String toString() {
        return "PasswordParameter{" +
                "maxLimitPasswordAction=" + maxLimitPasswordAction +
                ", minLimitPasswordAction=" + minLimitPasswordAction +
                ", minLengthPassword=" + minLengthPassword +
                ", regex='" + regex + '\'' +
                ", isUniquePassword=" + isUniquePassword +
                '}';
    }
}
