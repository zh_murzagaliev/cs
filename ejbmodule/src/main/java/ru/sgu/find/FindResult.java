package ru.sgu.find;


import java.io.Serializable;
import java.util.Date;

public class FindResult implements Serializable {
    private String fileName;
    private Long length;
    private Date lastModified;
    private String path;
    private String hash;

    public FindResult(String fileName, Long length, Date lastModified, String path, byte [] hash) {
        this.fileName = fileName;
        this.lastModified = lastModified;
        this.path = path;
        this.hash = bytesToHexString(hash);
        this.length = length;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    private  String bytesToHexString(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for(byte b : bytes){
            sb.append(String.format("%02x", b&0xff));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "FindResult{" +
                "fileName='" + fileName + '\'' +
                ", length=" + length +
                ", lastModified=" + lastModified +
                ", path='" + path + '\'' +
                ", hash='" + hash + '\'' +
                '}';
    }
}
