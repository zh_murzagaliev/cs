package ru.sgu.find;
import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameCriterion implements FileCriterion {

    PathMatcher pathMatcher;
    String fileName;

    public NameCriterion(String fileName) {
        this.fileName = fileName;
        this.pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + fileName);
    }

    @Override
    public boolean isGoodFile(File file) {
        return pathMatcher.matches(file.toPath().getFileName());
    }

}
