package ru.sgu.find;

import java.io.File;
import java.util.Date;

public class DateMinModifiedCriterion implements FileCriterion {
    private Date minModifiedDate;

    public DateMinModifiedCriterion(Date minModifiedDate) {
        this.minModifiedDate = minModifiedDate;
    }

    @Override
    public boolean isGoodFile(File file) {
        boolean result = true;
        Date lastModifiedDate = new Date(file.lastModified());
        if (minModifiedDate != null && lastModifiedDate.getTime() < minModifiedDate.getTime()) {
            result = false;
        }
        return result;
    }

}
