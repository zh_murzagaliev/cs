package ru.sgu.find;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SubStringCriteria implements FileCriterion {

    private String findString;

    public SubStringCriteria(String findString) {
        this.findString = findString;
    }

    @Override
    public  boolean isGoodFile(File file) {
        boolean status = false;

        int lengthString = findString.length();
        int lengthContents = (int) file.length();

        FileInputStream fileInput = null;
        try {
            fileInput = new FileInputStream(file);
            byte [] contents = new byte[lengthContents];
            fileInput.read(contents);
            byte [] string = findString.getBytes();
            byte [] text = joinByteArrays(string, contents);

            int prefixFunction [] = prefixFunction(text, lengthString);

            for (int prf: prefixFunction) {
                if (prf == lengthString) {
                    status = true;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error:" + e);
        } catch (IOException e) {
            System.out.println("Error:" + e);
        } finally {
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
                    System.out.println("Error: " + e);
                }
            }
        }
        return status;
    }

    public  int[] prefixFunction (byte[] target, int l) {
        int n = target.length;
        int[] pi = new int[n];
        for (int i = 1; i < n; i++ ) {
            if (i == l) {
                pi[i] = 0;
                continue;
            }
            int j = pi[i - 1];
            while ( j > 0 && (target[i] != target[j] || j == l) )
                j = pi[j-1];
            if (target[i] == target[j] )  ++j;
            pi[i] = j;
        }
               return pi;
    }

    public byte[] joinByteArrays(byte[] first, byte[] second) {
        byte [] joinArr = new byte[first.length + second.length + 1];
        int index = 0;
        for (byte b: first) {
            joinArr[index++] = b;
        }

        joinArr[index++] = (byte) 97;

        for (byte b: second) {
            joinArr[index++] = b;
        }
        return joinArr;
    }

    public static void main(String[] args) {
        File file = new File("D:/Tests/Test/java/src/ru/zhaslan/Hello.java");
        new SubStringCriteria("void").isGoodFile(file);
    }

}
