package ru.sgu.find;

import javax.ejb.Local;
import java.io.File;
import java.util.List;

@Local
public interface Finder {
    public  List<File> findFilesByCriterion(String dir, List<FileCriterion> criteria);
    public void saveFindResult(String destination, List<FindResult> findResult, boolean  isCrypto, String key);
    public List<FindResult> openFindResult(String path, boolean isCrypto, String key);
}
