package ru.sgu.find;


import java.io.File;

public class SizeMinCriterion implements FileCriterion {

    private Long minSize;

    public SizeMinCriterion(Long minSize) {
        this.minSize = minSize;
    }

    @Override
    public boolean isGoodFile(File file) {
        boolean result = true;

        if (minSize != null && file.length() < minSize) {
            result = false;
        }
        return result;
    }
}
