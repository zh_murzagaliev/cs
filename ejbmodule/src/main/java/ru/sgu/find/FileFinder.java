package ru.sgu.find;
import ru.sgu.crypto.Crypto;

import javax.crypto.Cipher;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

@Stateless
public class FileFinder implements Finder {

    @EJB
    Crypto crypto;

    public boolean isGoodFileByCriteria(File file, List<FileCriterion> criteria) {
        boolean status = true;
        for (FileCriterion criterion: criteria) {
            if (!criterion.isGoodFile(file)) {
                status = false;
            }
        }
        return status;
    }

    public List<File> findFilesByCriterion(String dir, List<FileCriterion> criteria){
        List<File> resultList = new ArrayList<File>();

        Stack<String> stack = new Stack<String>();
        stack.push(dir);

        while (!stack.empty()) {
            File nextDir = new File(stack.pop());
            File [] tempFiles = nextDir.listFiles();
            if (tempFiles == null) {
                continue;
            }
            for (File file: tempFiles) {
                if (file.isFile()) {
                    if (isGoodFileByCriteria(file, criteria))
                    resultList.add(file);
                }
                if (file.isDirectory()) {
                    stack.push(file.getAbsolutePath());
                }
            }
        }
        return resultList;
    }

    public void saveFindResult(String destination, List<FindResult> findResult, boolean  isCrypto, String key) {

        File file = new File(destination);
        try(ObjectOutputStream objectOutput = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutput.writeObject(findResult);

        } catch (FileNotFoundException e) {
            System.out.println("Error:" + e);
        } catch (IOException e) {
            System.out.println("Error" + e);
        }

        if (isCrypto && key != null) {
            crypto.cryptographyAES(destination, destination,key, Cipher.ENCRYPT_MODE);
        }
    }

    @Override
    public List<FindResult> openFindResult(String path, boolean isCrypto, String key) {
        File file = new File(path);
        if (isCrypto) {
            crypto.cryptographyAES(path, path, key, Cipher.DECRYPT_MODE);
        }
        List<FindResult> results = null;
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
             results = (List<FindResult>) objectInputStream.readObject();
        } catch (ClassNotFoundException | IOException e) {
            System.out.println("Error:" + e);
        }
        return results;
    }
}
