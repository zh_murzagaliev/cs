package ru.sgu.find;
import java.io.File;
import java.util.Date;

public class DateMaxModifiedCriterion implements FileCriterion {

    private Date maxModifiedDate;

    public DateMaxModifiedCriterion(Date maxModifiedDate) {
        this.maxModifiedDate = maxModifiedDate;
    }

    @Override
    public boolean isGoodFile(File file) {
        boolean result = true;
        Date lastModifiedDate = new Date(file.lastModified());
        if (maxModifiedDate != null && lastModifiedDate.getTime() > maxModifiedDate.getTime()) {
            result = false;
        }
        return result;
    }
}
