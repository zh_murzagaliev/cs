package ru.sgu.find;
import java.io.File;

public interface FileCriterion {
    public boolean isGoodFile(File file);
}
