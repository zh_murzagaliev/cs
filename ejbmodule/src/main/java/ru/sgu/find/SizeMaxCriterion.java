package ru.sgu.find;
import java.io.File;

public class SizeMaxCriterion implements FileCriterion {

    private Long maxSize;

    public SizeMaxCriterion(Long maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean isGoodFile(File file) {
        boolean result = true;

        if (maxSize != null && file.length() > maxSize) {
            result = false;
        }
        return result;
    }
}
