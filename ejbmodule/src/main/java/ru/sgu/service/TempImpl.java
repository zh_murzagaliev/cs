package ru.sgu.service;

import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

@SecurityDomain("test")
@Stateless
public class TempImpl implements Temp {

    @Resource
    SessionContext session;

    @RolesAllowed("ADMINd")
    @Override
    public boolean go() {
        String pr = session.getCallerPrincipal().getName();

        System.out.println(pr);
        return true;
    }
}
