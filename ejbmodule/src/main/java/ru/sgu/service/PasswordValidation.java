package ru.sgu.service;

import javax.ejb.Local;

@Local
public interface PasswordValidation {
    public boolean validate(String password);
}
