package ru.sgu.service;


import ru.sgu.domain.User;

import javax.ejb.Local;
import java.util.List;

@Local
public interface UserManager {
    public void createUser(User user);
    public void removeUser(Long userId);
    public void updateUser(User user);
    public List<User> findAll();
    public User findById(Long id);
    public boolean check();
}
