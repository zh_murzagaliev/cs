package ru.sgu.service;


import ru.sgu.domain.PasswordParameter;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.regex.Pattern;

@Stateless
public class PasswordValidationImpl implements PasswordValidation {

    @EJB
    PasswordParameterManager parameterManager;


    private final double PASSWORD_PROBABILITY_MAX = 0.5;

    Pattern digit = Pattern.compile("[0-9]");
    Pattern alpha = Pattern.compile("[a-zA-Z]");
    Pattern punct = Pattern.compile("\\W");


    public boolean validate(String password){
        boolean ok = true;
        double length = password.length();
        double powerAbc = 0;
        if (isPatternOk(password, digit)) {
            powerAbc += 10;
        }
        if (isPatternOk(password, alpha)) {
            powerAbc += 54;
        }
        if (isPatternOk(password, punct)) {
            powerAbc += 33;
        }

        PasswordParameter parameter = parameterManager.getPasswordParameter();

        double speed = parameter.getSpeedOfPasswordGuessing();
        double time =  (parameter.getMaxLimitPasswordAction().getTime() - new Date(System.currentTimeMillis()).getTime())/6000 ;
        System.out.println("!!" + powerAbc + " " + length);
        double power = Math.pow(powerAbc, length);
        double probability = (speed * time) / power;

        System.out.println(speed + " " + time + " " + power + " " + probability);

        ok &= probability < PASSWORD_PROBABILITY_MAX;
        ok &= length < parameter.getMinLengthPassword();

        return ok;
    }

    private boolean isPatternOk(String password, Pattern pattern) {
        boolean ok = false;
        for (char c: password.toCharArray()) {
            ok |= pattern.matcher(String.valueOf(c)).matches();
        }
        return ok;
    }

    public static void main(String[] args) {
        new PasswordValidationImpl().validate("pass1");
    }
}
