package ru.sgu.service;

import javax.ejb.Local;

@Local
public interface Temp {
    public boolean go();
}
