package ru.sgu.service;

import ru.sgu.crypto.Crypto;
import ru.sgu.domain.PasswordParameter;

import javax.crypto.Cipher;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Stateless
public class PasswordParameterManagerImpl implements PasswordParameterManager {

    @EJB
    Crypto crypto;

    private  final String pathProp = "d:/parameter.properties".intern();



    private Properties load(){
        crypto.cryptographyAES(pathProp, pathProp, "123", Cipher.DECRYPT_MODE);
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(pathProp);
        } catch (FileNotFoundException e) {
            System.out.println("Error:" + e);
        }

        Properties properties = new Properties();
        try {
            properties.load(fileInputStream);
        } catch (IOException e) {
            System.out.println("Error!!!!" + e);
        }
        crypto.cryptographyAES(pathProp, pathProp, "123", Cipher.ENCRYPT_MODE);
        return properties;
    }

    @Override
    public PasswordParameter getPasswordParameter() {
        Properties properties = load();
        PasswordParameter parameter = buildPasswordParameter(properties);
        return  parameter;
    }

    private PasswordParameter buildPasswordParameter(Properties properties) {
        return new PasswordParameter(
                parseDate(properties.getProperty("max_limit_password_action")),
                parseDate(properties.getProperty("min_limit_password_action")),
                Integer.parseInt(properties.getProperty("min_length_password")),
                properties.getProperty("regex"),
                Boolean.parseBoolean(properties.getProperty("is_unique_password")),
                Integer.parseInt(properties.getProperty("speed_of_password_guessing")) );
    }

    @Override
    public void setPasswordParameter(PasswordParameter parameter) {
        Properties properties = new Properties();
        properties.setProperty("max_limit_password_action", parseDateToString(parameter.getMaxLimitPasswordAction()));
        properties.setProperty("min_limit_password_action", parseDateToString(parameter.getMinLimitPasswordAction()));
        properties.setProperty("min_length_password", String.valueOf(parameter.getMinLengthPassword()));
        properties.setProperty("speed_of_password_guessing", String.valueOf(parameter.getSpeedOfPasswordGuessing()));
        properties.setProperty("regex", String.valueOf(parameter.getRegex()));
        properties.setProperty("is_unique_password", String.valueOf(parameter.isUniquePassword()));
        crypto.cryptographyAES(pathProp, pathProp, "123", Cipher.DECRYPT_MODE);
        try {
            OutputStream outputStream = new FileOutputStream(pathProp);
            properties.store(outputStream,"!!!");
        } catch (FileNotFoundException e) {
            System.out.println("Error:" + e);
        } catch (IOException e) {
            System.out.println("Error:" + e);
        }
        crypto.cryptographyAES(pathProp, pathProp, "123", Cipher.ENCRYPT_MODE);
    }

    public static void main(String[] args) {
        String s1 = "dad23ds";
        System.out.println(s1.contains("2"));
    }

    private Date parseDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm");
        Date resultDate = null;
        try {
            resultDate = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date parse error:" + e);
        }
        return resultDate;
    }


    private String parseDateToString (Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm");
        return dateFormat.format(date);
    }


}
