package ru.sgu.service;

import ru.sgu.domain.PasswordParameter;

import javax.ejb.Local;

@Local
public interface PasswordParameterManager {
    public PasswordParameter getPasswordParameter();
    public void setPasswordParameter(PasswordParameter parameter);
}
