package ru.sgu.service;

import ru.sgu.domain.User;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;


@Stateless(name = "UserManager")
public class UserManagerImpl implements UserManager {

    private static final String SUPER_ROOT = "admin";
    @PersistenceContext(unitName = "modelXa")
    EntityManager em;

    @Resource
    SessionContext sessionContext;

    @Override
    public void createUser(User user) {
        em.persist(user);
    }

    @Override
    public void removeUser(Long userId) {
        User user = em.find(User.class, userId);
        if (!user.getUserName().equals(SUPER_ROOT))  {
            em.remove(user);
        }
    }

    @Override
    public void updateUser(User user) {
        User userUp = em.find(User.class, user.getId());
        userUp.setRole(user.getRole());
        userUp.setUserName(user.getUserName());
        if (!user.getUserPassword().equals(userUp.getUserPassword()))  {
            userUp.setUserPassword(user.getUserPassword());
            userUp.setDateCreatingPassword(new Date(System.currentTimeMillis()));
        }
    }

    @Override
    public List<User> findAll() {
        return em.createNamedQuery("User.findAll").getResultList();
    }

    @Override
    public User findById(Long id) {
        return em.find(User.class, id);
    }

    @Override
    public boolean check() {
        String s = sessionContext.getCallerPrincipal().getName();
        return false;
    }


}