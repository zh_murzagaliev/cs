package com.zhaslan;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit send for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the send case
     *
     * @param testName name of the send case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous TestAsynch :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
