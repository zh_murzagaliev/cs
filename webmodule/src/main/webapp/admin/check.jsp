<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Клиенты</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>

<section class="container">
    <div class="login">
        <h1>Вы зашли от администратора  <a href ="/webmodule/check"> Выйти </a> </h1>
        <form action = "add" method = "post">
            <input type="password" name="user_password" placeholder = "Пароль" required="required"> <br />
            <input type="password" name="user_password_repeat" placeholder = "Повторите пароль" required="required"> <br />
            <input type="text" name="speed" placeholder = "Скорость подбора пароля(шт/мин)" required="required"> <br />
            <input type="text" name="power_abc" placeholder = "Мощность алфавита пароля" required="required"> <br />
            <input type="text" name="other1" readonly placeholder = "мощность пространства паролей(авто)"> <br />
            <input type="text" name="spother2" readonly placeholder = "Длина пароля (авто)"> <br />
            <br />
            <center>
                <input type="submit" value="Отправить">
            </center>
        </form>
    </div>
</section>
</body>
</html>

