<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
          <title>Клиенты</title>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" href="style.css">
    </head>

    <body>

        <section class="container">
             <div class="login">
                <h1>Вы зашли от администратора  <a href ="/webmodule/logout"> Выйти </a> </h1>
                    <a href ="/webmodule/admin/list.jsp"><input type="submit" value="Редактирование пользователей"></a>
                    <br />
                    <br />
                    <a href ="/webmodule/admin/passwordsystemr"><input type="submit" value="Параметры парольной системы"></a>
                    <br />
                    <br />
                    <a href ="/webmodule/admin/check.jsp"><input type="submit" value="Оценить сложность пароля"> </a>
                    <br />
                    <br />
                    <a href ="/webmodule/admin/find.jsp"><input type="submit" value="Поиск файлов"> </a>
                 <br />
                 <br />
                 <a href ="/webmodule/admin/crypto.jsp"><input type="submit" value="Модуль криптозащиты файлов"> </a>

                 </form>
             </div>
        </section>
    </body>
</html>

