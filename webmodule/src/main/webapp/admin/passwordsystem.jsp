<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
          <title>Клиенты</title>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" href="style.css">
    </head>

    <body>

        <section class="container">
             <div class="login">
                <h1>Парольная система </h1>
                    <form action = "/webmodule/admin/passwordsystemc" method = "post">
                        <%DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm");%>
                        Макс срок действия пароля <br /><input type="date" name="max_action_password_date"  value="<%= request.getAttribute("max_action_password_date")%>"  required="required">
                        <input type="time" name="max_action_password_time" value="<%= request.getAttribute("max_action_password_time")%>"  required="required"><br />
                    Мин  срок действия пароля <br /> <input type="date" name="min_action_password_date" value="<%= request.getAttribute("min_action_password_date")%>"   required="required">
                        <input type="time" name="min_action_password_time" value="<%= request.getAttribute("min_action_password_time")%>"  required="required"> <br />
                    <br /><input type="text" name="min_length_password" placeholder = "Мин длина пароля"  value="<%= request.getAttribute("min_length_password")%>"   required="required"> <br />
                        <br /><input type="text" name="regex" placeholder = "Регулярное выражение"  value="<%= request.getAttribute("regex")%>"  required="required"> <br />
                        <br /><input type="text" name="speed_of_password_guessing" placeholder = "Скорость подбора пароля"  value="<%= request.getAttribute("speed_of_password_guessing")%>"  required="required"> <br />


                        <br />
                    <br />
                    <center>
                        <input type="submit" value="Изменить">
                    </center>
                </form>
             </div>
        </section>
    </body>
</html>

