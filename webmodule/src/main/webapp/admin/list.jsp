<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*, java.text.*, ru.sgu.domain.User" %>


<html>
<head>
    <title>Клиенты</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>

<section class="container">
    <div class="login">
        <h1>Редактирование пользователей </h1>
        <table border = "4">
            <tr>
                <td> ID клиента </td>
                <td> Имя пользователя </td>
                <td> Категория прав </td>

            </tr>

            <jsp:useBean id="logic" class="ru.servlet.other.LogicBean" />
            <c:set var="users" value="${logic.users}" />
            <c:forEach items="${users}" var="user">
                <tr>
                    <td><c:out value="${user.id}"/></td>
                    <td><c:out value="${user.userName}"/></td>
                    <td><c:out value="${user.role.userRole}"/></td>
                    <td><a href = "/webmodule/admin/delete?user_id=${user.id}"> <input type="submit" value="X"> </a></td>
                    <td><a href = "/webmodule/admin/info.jsp?user_id=${user.id}"> <input type="submit" value="*"> </a></td>
                </tr>
            </c:forEach>
        </table>
        <br />
        <br />
        <a href ="/webmodule/admin/formclient.jsp">
            <input type="submit" value="Добавить клиента">
        </a>

    </div>
</section>
</body>
</html>

