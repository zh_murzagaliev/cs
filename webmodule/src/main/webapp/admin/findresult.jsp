<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Клиенты</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>

<body>
<center>
   <table width = "10%">
            <tr align = "center">
                <td >Имя файла</td>
                <td>Размер в байтах</td>
                <td>Абсолютный путь</td>
                <td>Дата последнего изменения</td>
                <td>MD5</td>
            </tr>
            <c:forEach items="${results}" var="result">
                <tr align = "center">
                    <td><c:out value="${result.fileName}"/></td>
                    <td><c:out value="${result.length}"/></td>
                    <td><c:out value="${result.path}"/></td>
                    <td><c:out value="${result.lastModified}"/></td>
                    <td><c:out value="${result.hash}"/></td>
                </tr>
            </c:forEach>


        </table>
</center>
</body>
</html>

