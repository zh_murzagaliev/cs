<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*, java.text.*, ru.sgu.domain.User" %>
<%@ page import="ru.sgu.domain.RoleType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Клиенты</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<section class="container">
    <div class="login">
        <h1>Редактирование пользователя </h1>

        <jsp:useBean id="logic" class="ru.servlet.other.LogicBean" />

        <%User user = logic.findUserById(request.getParameter("user_id"));
            if (user == null) {
            } else {
        %>
        <form action = "/webmodule/admin/update" method = "post">
            ID пользователя <input type="text" readonly name="user_id"  required="required" value = "<%=user.getId()%>"> <br />
            Имя пользователя <input type="text" name="user_name" required="required" value = "<%=user.getUserName()%>"> <br />
            Пароль <input type="password" name="user_password" placeholder = "Новый пароль" required="required"> <br />
            Еще раз<input type="password" name="user_password_repeat" placeholder = "Повторите пароль" required="required"> <br />
            Категория прав
            <select name="user_role">
                <%if (user.getRole().getUserRole() == RoleType.ADMIN ) {
                %>
                <option selected>ADMIN</option>
                <option>USER</option>
                <%} else {%>
                <option>ADMIN</option>
                <option selected>USER</option>
                <%}%>

            </select>

            <br />

            <input type="submit" value="Обновить">
        </form>
        <%          }
        %>

    </div>
</section>

</body>
</html>

