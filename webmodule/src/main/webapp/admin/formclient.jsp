<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
          <title>Клиенты</title>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" href="style.css">
    </head>

    <body>

        <section class="container">
             <div class="login">
                <h1>Регистрация нового пользователя</h1>
                    <form action = "add" method = "post">
                    <input type="text" name="user_name" placeholder = "Имя пользователя"    required="required"> <br />
                    <input type="password" name="user_password" placeholder = "Пароль" required="required"> <br />
                    <input type="password" name="user_password_repeat" placeholder = "Повторите пароль" required="required"> <br />
                    Роль <select name = "user_role">
                            <option>ADMIN</option>
                            <option>USER</option>
                         </select>
                    <br />
                    <br />
                    <center>
                        <input type="submit" value="Отправить">
                    </center>
                </form>
             </div>
        </section>
    </body>
</html>

