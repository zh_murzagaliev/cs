<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
          <title>Клиенты</title>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="stylesheet" href="style.css">
    </head>

    <body>

        <section class="container">
             <div class="login">
                <h1>Вы зашли от администратора  <a href ="/webmodule/logout"> Выйти </a> </h1>

                 <form action = "find" method = "post">

                     <input type="text" name="file_dir" placeholder = "Директория"    required="required"> <br />

                     <input type="text" name="file_name" placeholder = "Имя файла или шаблон"   > <br />
                     Макс дата изменения <br /><input type="date" name="max_date_change"  >
                     <input type="time" name="max_time_change"  ><br />
                     Мин  дата изменения <br /> <input type="date" name="min_date_change"  >
                     <input type="time" name="min_time_change"  > <br />
                     <input type="text" name="file_max_size" placeholder = "Макс размер файла" > <br />
                     <input type="text" name="file_min_size" placeholder = "Мин размер файла" > <br />
                     <input type="text" name="sub_string" placeholder = "Подстрока" > <br />
                     Сохранить <input type="checkbox" name="is_save" > <br />
                     <input type="text" name="destination" placeholder = "в этом файле" > <br />
                     и при этом шифровать<input type="checkbox" name="is_crypto"  > <br />
                     <input type="password" name="key" placeholder = "ключом" > <br />

                     <input type="submit" value="Отправить">
                 </form>
                 <a href="/webmodule/admin/openfind.jsp"><input type="submit" value="Открыть результаты поиска"> </a>
             </div>
        </section>
    </body>
</html>

