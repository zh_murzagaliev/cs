package ru.servlet.passwordmodule;

import ru.servlet.other.Utils;
import ru.sgu.domain.PasswordParameter;
import ru.sgu.service.PasswordParameterManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = {"/admin/passwordsystemc"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class PasswordSystem extends HttpServlet {

    @EJB
    PasswordParameterManager manager;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String maxDate = request.getParameter("max_action_password_date");
        String maxTime = request.getParameter("max_action_password_time");
        Date max_action_password = parseDate(maxDate + "." + maxTime);

        String minDate = request.getParameter("min_action_password_date");
        String minTime = request.getParameter("min_action_password_time");
        int speed = Integer.parseInt(request.getParameter("speed_of_password_guessing"));
        Date min_action_password = parseDate(minDate + "." + minTime);

        Date currentDate = new Date(System.currentTimeMillis());

        if ( (max_action_password.getTime() < currentDate.getTime())
                || (currentDate.getTime() > min_action_password.getTime()) ) {
            new Utils(request, response).goStatusPage("/webmodule/admin/passwordsystem.jsp", "Такого быть не может","Назад");
            return;
        }

        int minLength = Integer.parseInt(request.getParameter("min_length_password"));
        String regex = request.getParameter("regex");

        PasswordParameter parameter = new PasswordParameter(max_action_password, min_action_password,
                minLength, regex, false, speed);

        manager.setPasswordParameter(parameter);
    }

    private Date parseDate(String date) {
        System.out.println(date);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm");
        Date resultDate = null;
        try {
            resultDate = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date parseLong error:" + e);
        }
        return resultDate;
    }
}
