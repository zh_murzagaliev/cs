package ru.servlet.passwordmodule;

import ru.servlet.other.Utils;
import ru.sgu.domain.PasswordParameter;
import ru.sgu.service.PasswordParameterManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = {"/admin/passwordsystemr"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class ReadPasswordSystem extends HttpServlet {

    @EJB
    PasswordParameterManager manager;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PasswordParameter parameter = manager.getPasswordParameter();
        request.setAttribute("min_length_password", parameter.getMinLengthPassword());
        request.setAttribute("regex", parameter.getRegex());

        request.setAttribute("max_action_password_date", parseDateToString(parameter.getMaxLimitPasswordAction()));
        request.setAttribute("max_action_password_time", parseTimeToString(parameter.getMaxLimitPasswordAction()));

        request.setAttribute("min_action_password_date", parseDateToString(parameter.getMinLimitPasswordAction()));
        request.setAttribute("min_action_password_time", parseTimeToString(parameter.getMinLimitPasswordAction()));
        request.setAttribute("speed_of_password_guessing", parameter.getSpeedOfPasswordGuessing());

        request.getRequestDispatcher("/admin/passwordsystem.jsp").forward(request, response);

    }
    private  String parseDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String resultDate = null;
        resultDate = dateFormat.format(date);
        return resultDate;
    }

    private  String parseTimeToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String resultDate = null;
        resultDate = dateFormat.format(date);
        return resultDate;
    }

}
