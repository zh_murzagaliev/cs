package ru.servlet.other;

import ru.sgu.domain.User;
import ru.sgu.service.UserManager;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.List;

@SuppressWarnings("unused")
public class LogicBean {

    private User user;

    private UserManager getUserManager() {
        UserManager userManager = null;
        try {
            InitialContext ctx = new InitialContext();
            userManager = (UserManager) ctx.lookup("java:global/app/EjbModule/UserManager");
        } catch (NamingException e) {
            System.out.println("Error: " + e);
        }
        return userManager;
    }

    public List<User> getUsers() {
        UserManager userManager = getUserManager();
        List<User> users = null;

        if (userManager != null) {
            users = userManager.findAll();
        }
        return users;
    }


    public User getUser() {
        return user;
    }

    public User findUserById(String user_id) {
        UserManager userManager = getUserManager();
        return userManager.findById(Long.parseLong(user_id));
    }
}
