package ru.servlet.other;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private  final String pathProp = "../../route.properties";
    private  Properties properties;

    public Utils(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }

    public Utils() {
    }

    public void goStatusPage( String backUrl, String statusMessage, String buttonTitle) throws ServletException, IOException {
        request.setAttribute("back_url", backUrl);
        request.setAttribute("status", statusMessage);
        request.setAttribute("button_title", buttonTitle);

        request.getRequestDispatcher("/webmodule/admin/status.jsp").forward(request, response);
    }

    public Properties getRouteTable(){
        FileInputStream fileInputStream = (FileInputStream) getClass().getResourceAsStream(pathProp);
        properties = new Properties();
        try {
            properties.load(fileInputStream);
        } catch (IOException e) {
            System.out.println("Error" + e);
        }
        return properties;
    }

    public  String getRouteUnit(String key) {
        if (properties == null) {
            properties = getRouteTable();
        }
        return properties.getProperty(key);
    }

}
