package ru.servlet.other;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParseUtils {
    public static Date parseDateTime(String date) {
        System.out.println(date);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HH:mm");
        Date resultDate = null;
        try {
            resultDate = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date parseLong error:" + e);
        }
        return resultDate;
    }

    public static Long parseLong(String s) {
        Long ans = null;
        try {
            ans = Long.parseLong(s);
        }   catch (NumberFormatException ignored) {
        }
        return ans;
    }


}
