package ru.servlet.loginmodule;


import ru.sgu.service.Temp;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/index"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN", "USER"}))
public class LogIn extends HttpServlet {

    @EJB
    Temp temp;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        httpServletResponse.sendRedirect("/webmodule/admin/console.jsp");
    }
}
