package ru.servlet.filemodule;

import ru.servlet.other.ParseUtils;
import ru.sgu.crypto.Crypto;
import ru.sgu.find.*;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = {"/admin/openfind"})
public class OpenFindServlet extends HttpServlet {

    @EJB
    Finder finder;

    @EJB
    Crypto crypto;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path = request.getParameter("path");

        boolean isCrypto = "on".equals(request.getParameter("is_crypto"));

        String key = request.getParameter("key");

        List<FindResult> results = finder.openFindResult(path, isCrypto, key);
        request.setAttribute("results", results);
        request.getRequestDispatcher("/admin/findresult.jsp").forward(request, response);

    }
}
