package ru.servlet.filemodule;

import ru.servlet.other.Utils;
import ru.sgu.crypto.Crypto;

import javax.crypto.Cipher;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/admin/crypto"})
public class CryptoServlet extends HttpServlet {

    @EJB
    Crypto crypto;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String source = request.getParameter("source");
        String destination = request.getParameter("destination");
        String key = request.getParameter("key");
        String mode = request.getParameter("mode");
        if ("Encrypt".equals(mode)) {
            crypto.cryptographyAES(source, destination, key, Cipher.ENCRYPT_MODE );
        } else {
            crypto.cryptographyAES(source, destination, key, Cipher.DECRYPT_MODE);
        }
        response.sendRedirect("/webmodule/admin/crypto.jsp");
    }
}
