package ru.servlet.filemodule;

import ru.servlet.other.ParseUtils;
import ru.sgu.crypto.Crypto;
import ru.sgu.find.*;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(urlPatterns = {"/admin/find"})
public class FinderServlet extends HttpServlet {

    @EJB
    Finder finder;

    @EJB
    Crypto crypto;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String fileDir = request.getParameter("file_dir");

        List<FileCriterion> criteria = new ArrayList<>();
        String fileName = request.getParameter("file_name");
        if (fileName != null && !fileName.equals("")) {
            criteria.add(new NameCriterion(fileName));
        }
        String maxDateChange = request.getParameter("max_date_change");
        String maxTimeChange = request.getParameter("max_time_change");
        Date maxModified = ParseUtils.parseDateTime(maxDateChange + "." + maxTimeChange);
        if (maxModified != null) {
            criteria.add(new DateMaxModifiedCriterion(maxModified));
        }

        String minDateChange = request.getParameter("min_date_change");
        String minTimeChange = request.getParameter("min_time_change");
        System.out.println(maxDateChange + " " + minTimeChange);
        Date minModified = ParseUtils.parseDateTime(minDateChange + "." + minTimeChange);
        if (minModified != null) {
            criteria.add(new DateMinModifiedCriterion(minModified));
        }

        Long fileMaxSize = ParseUtils.parseLong(request.getParameter("file_max_size"));
        if ( fileMaxSize!= null) {
            criteria.add(new SizeMaxCriterion(fileMaxSize));
        }

        Long fileMinSize = ParseUtils.parseLong(request.getParameter("file_min_size"));
        if (fileMinSize != null) {
            criteria.add(new SizeMinCriterion(fileMinSize));
        }

        String subString = request.getParameter("sub_string");
        if (subString != "") {
            criteria.add(new SubStringCriteria(subString));
        }


        List<File> fileList = finder.findFilesByCriterion(fileDir, criteria);
        List<FindResult> results = new ArrayList<>();
        for (File file: fileList) {
            results.add(new FindResult(file.getName(), file.length(), new Date(file.lastModified()),
                    file.getAbsolutePath(), crypto.getMD5File(file)));
        }


        boolean isSave = "on".equals(request.getParameter("is_save"));

        System.out.println(isSave);

        if (isSave) {
            String destination = request.getParameter("destination");
            boolean isCrypto = "on".equals(request.getParameter("is_crypto"));
            String key = null;
            if (isCrypto) {
                key = request.getParameter("key");
            }
            finder.saveFindResult(destination, results, isCrypto, key);
        }
        request.setAttribute("results", results);
        request.getRequestDispatcher("/admin/findresult.jsp").forward(request, response);
    }
}