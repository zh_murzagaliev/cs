package ru.servlet.usermanagermodule;

import ru.servlet.other.Utils;
import ru.sgu.domain.Role;
import ru.sgu.domain.RoleType;
import ru.sgu.domain.User;
import ru.sgu.service.UserManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet(urlPatterns = {"/admin/update"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class ServletUpdate extends HttpServlet {

    @EJB
    private UserManager userManager;

    Utils util;

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        util = new Utils(httpServletRequest, httpServletResponse);
        User user = buildClientFromRequestParameters(httpServletRequest);
        if (user != null) {
            userManager.updateUser(user);
        }
        httpServletResponse.sendRedirect("/webmodule/admin/list.jsp");
    }

    User buildClientFromRequestParameters(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        String userName = httpServletRequest.getParameter("user_name");
        String userPassword = httpServletRequest.getParameter("user_password");
        Long id = Long.parseLong(httpServletRequest.getParameter("user_id"));
        String userPasswordRepeat = httpServletRequest.getParameter("user_password_repeat");
        if (!userPassword.equals(userPasswordRepeat)) {
            util.goStatusPage("/webmodule/admin/list.jsp", "Пароли не совпадают", "К странице редактирования");
            return null;
        }
        RoleType roleType = RoleType.valueOf(httpServletRequest.getParameter("user_role"));
        Role role = new Role(roleType);
        User user = new User(userName, userPassword, new Date(System.currentTimeMillis()),role);
        user.setId(id);
        return user;
    }
}
