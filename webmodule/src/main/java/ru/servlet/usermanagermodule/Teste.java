package ru.servlet.usermanagermodule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Teste {

    public static void main(String[] args) {
        Date date = new Date(System.currentTimeMillis());
        String s = parseTimeToString(date);
        System.out.println(s);
    }
    private static String parseDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String resultDate = null;
        resultDate = dateFormat.format(date);
        return resultDate;
    }

    private static String parseTimeToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String resultDate = null;
        resultDate = dateFormat.format(date);
        return resultDate;
    }
}
