package ru.servlet.usermanagermodule;

import ru.servlet.other.Utils;
import ru.sgu.domain.Role;
import ru.sgu.domain.RoleType;
import ru.sgu.domain.User;
import ru.sgu.service.PasswordValidation;
import ru.sgu.service.UserManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

@WebServlet(urlPatterns = {"/admin/add"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class ServletAdd extends HttpServlet {

    @EJB
    private UserManager userManager;

    @EJB
    PasswordValidation passwordValidation;

    Utils util;

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
       util = new Utils(httpServletRequest, httpServletResponse);
       User user = buildClientFromRequestParameters(httpServletRequest);

       httpServletResponse.sendRedirect("/webmodule/admin/list.jsp");
    }

    User buildClientFromRequestParameters(HttpServletRequest httpServletRequest) throws ServletException, IOException {
        String userName = httpServletRequest.getParameter("user_name");
        String userPassword = httpServletRequest.getParameter("user_password");
        String userPasswordRepeat = httpServletRequest.getParameter("user_password_repeat");
        System.out.println(userPassword + " " + userPasswordRepeat);
        if (!userPassword.equals(userPasswordRepeat)) {
            util.goStatusPage("/webmodule/admin/list.jsp", "Пароли не совпадают", "Еще раз");

        }

      /*  if (!passwordValidation.validate(userPassword)) {
            util.goStatusPage("/webmodule/admin/list.jsp", "Пароль не прошла валидацию", "Еще раз");
        }
*/
        RoleType roleType = RoleType.valueOf(httpServletRequest.getParameter("user_role"));
        Role role = new Role(roleType);
        MessageDigest md = null;
        try {
             md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error");
        }
        User user = new User(userName, decode(userPassword), new Date(System.currentTimeMillis()),role);
        return user;
    }

    public String decode(String message) {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hash = md.digest(message.getBytes("UTF-8")); //converting byte array to Hexadecimal String

            StringBuilder sb = new StringBuilder(2*hash.length);

            for(byte b : hash){ sb.append(String.format("%02x", b&0xff));

            }
            digest = sb.toString();
        } catch (UnsupportedEncodingException ex) {
            System.out.println("Error: " + ex);
        }
        catch (NoSuchAlgorithmException ex) {
            System.out.println("Error: " + ex);
        }
        return digest;
    }

}
