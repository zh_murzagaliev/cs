package ru.servlet.usermanagermodule;

import ru.servlet.other.Utils;
import ru.sgu.service.UserManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/admin/add"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class CheckServlet extends HttpServlet {

    @EJB
    private UserManager userManager;

    Utils util;

    @Override
    protected void doPost(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        util = new Utils(httpServletRequest, httpServletResponse);

        String userPassword = httpServletRequest.getParameter("user_password");
        String userPasswordRepeat = httpServletRequest.getParameter("user_password_repeat");

        if (!userPassword.equals(userPasswordRepeat)) {
            util.goStatusPage(util.getRouteUnit("form_client"), "Пароли не совпадают", "Еще раз");
            return;
        }

        int speed = Integer.parseInt(httpServletRequest.getParameter("speed"));
        int powerAbc = Integer.parseInt(httpServletRequest.getParameter("power_abc"));

        int length = userPassword.length();

        httpServletResponse.sendRedirect("/webmodule/admin/list.jsp");
    }


}
