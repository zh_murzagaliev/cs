package ru.servlet.usermanagermodule;


import ru.sgu.service.UserManager;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/delete")
@ServletSecurity(@HttpConstraint(rolesAllowed = {"ADMIN"}))
public class ServletDelete extends HttpServlet {

    @EJB
    private UserManager userManager;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userManager.removeUser(Long.parseLong(request.getParameter("user_id")));
        response.sendRedirect("/webmodule/admin/list.jsp");
    }
}
