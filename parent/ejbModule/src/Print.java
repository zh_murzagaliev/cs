import javax.ejb.Remote;

@Remote
public interface Print {
    public void print();
}
