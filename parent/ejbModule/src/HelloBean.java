import javax.ejb.Stateless;

@Stateless(name = "HelloBeanName")
public class HelloBean implements Print {

    public HelloBean() {
        System.out.println("Constructor HelloBean");
    }

    @Override
    public void print() {
        System.out.println("Hello, Java Bean");
    }
}
